import pandas as pd
import numpy as np
from scipy import stats
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from my_functions import *
from dateutil.parser import parse


RANDOM_STATE = 42

train = pd.read_csv("/home/null/PycharmProjects/Miriade/data/raw_data/dataset_stima.csv")
test = pd.read_csv("/home/null/PycharmProjects/Miriade/data/raw_data/dataset_previsione.csv")

# drop delle osservazioni con var NUMERO_CLICK_SITO con valori negativi
train.drop(train[train.NUMERO_CLICK_SITO < 0].index, inplace=True)

# drop outlier della var. NUMERO_CLICK_SITO
train.drop(train[train['NUMERO_CLICK_SITO'] > 11000].index, inplace=True)

# correzione anno 3000
anno_3000 = train['FINE_PUBBLICITA'].str.match('3000')
max_anno = np.max(train['FINE_PUBBLICITA'][[not i for i in anno_3000]])
train.loc[anno_3000, 'FINE_PUBBLICITA'] = max_anno

# train['TIPO_PUBBLICITA'] = train['TIPO_PUBBLICITA'].astype('int').astype('category')
# train['PUBBLICITA'] = train['PUBBLICITA'].astype('int').astype('category')

train['MACRO_TIPO_PUBBLICITA'] = train['MACRO_TIPO_PUBBLICITA'].astype('category')
train['FASCIA_ORARIA'] = train['FASCIA_ORARIA'].astype('category')
train['FASCIA_TEMPISTICA'] = train['FASCIA_TEMPISTICA'].astype('category')
train['CODICE_GIOCO'] = train['CODICE_GIOCO'].astype('category')
train['RILANCIO_PUBBLICITA'] = train['RILANCIO_PUBBLICITA'].astype('int')
train['SESSIONE'] = train['SESSIONE'].astype('int').astype('category')
train['MACRO_CATEGORIA_GIOCO'] = train['MACRO_CATEGORIA_GIOCO'].astype('category')
train['MICRO_CATEGORIA_GIOCO'] = train['MICRO_CATEGORIA_GIOCO'].astype('category')
train['CLICK_PER_AREA'] = train['AREA_CLICK'].astype('int').astype('category')
train['NUMERO_PUBBLICITA_CONCORRENTI'] = train['NUMERO_PUBBLICITA_CONCORRENTI'].astype('category')
train['ID'] = train['ID'].astype('int').astype('category')

train['INIZIO_PUBBLICITA'] = pd.to_datetime(train['INIZIO_PUBBLICITA'], format='%Y-%m-%d')
train['FINE_PUBBLICITA'] = pd.to_datetime(train['FINE_PUBBLICITA'], format='%Y-%m-%d')
train['DATA'] = pd.to_datetime(train['DATA'], format='%Y-%m-%d')

train['INIZIO_PUBBLICITA'] = pd.DatetimeIndex(train['INIZIO_PUBBLICITA']).normalize()
train['FINE_PUBBLICITA'] = pd.DatetimeIndex(train['FINE_PUBBLICITA']).normalize()
train['DATA'] = pd.DatetimeIndex(train['DATA']).normalize()

# aggiustamento variabili INIZIO_PUBBLICTA e FINE_PUBBLICITA
flag = 0
for i, _ in train.iterrows():
    if train['INIZIO_PUBBLICITA'][i] > train['FINE_PUBBLICITA'][i]:
        flag = train['INIZIO_PUBBLICITA'][i]
        train.loc[i, 'INIZIO_PUBBLICITA'] = train['FINE_PUBBLICITA'][i]
        train.loc[i, 'FINE_PUBBLICITA'] = flag

# aggiustamento var DATA per rispettare i limiti delle var INIZIO_PUBBLICITA e FINE_PUBBLICITA
a = 0
for i, _ in train.iterrows():

    if train['DATA'][i] < train['INIZIO_PUBBLICITA'][i]:
        flag_inizio = train['INIZIO_PUBBLICITA'][i]
        train.loc[i, 'INIZIO_PUBBLICITA'] = train['DATA'][i]
        train.loc[i, 'DATA'] = flag_inizio

    if train['DATA'][i] > train['FINE_PUBBLICITA'][i]:
        flag_fine = train['FINE_PUBBLICITA'][i]
        train.loc[i, 'FINE_PUBBLICITA'] = train['DATA'][i]
        train.loc[i, 'DATA'] = flag_fine
'''
# (non ha molto senso) imputazione outlier per var. NUMERO_CLICK_SITO con valore medio
train.is_copy = False
media_NUMERO_CLICK_SITO = np.mean(train.NUMERO_CLICK_SITO[train.NUMERO_CLICK_SITO != -100])
train.NUMERO_CLICK_SITO[train.NUMERO_CLICK_SITO < 0] = media_NUMERO_CLICK_SITO

# imputazione valore modale alla var TIPO_PUBBLICTA
#moda_TIPO_PUBBLICITA = stats.mode(train['TIPO_PUBBLICITA'][train['TIPO_PUBBLICITA']!=pd.NaT])
#train['TIPO_PUBBLICITA'] = train['TIPO_PUBBLICITA'].fillna(int(moda_TIPO_PUBBLICITA.mode))
#train['TIPO_PUBBLICITA'].isnull().sum()
'''

X = train.iloc[:, [3, 4, 5, 8, 9, 10, 12, 13, 14, 15, 16]]   # 0, 6, 7 have missing data; 1, 2, 4 date vars; 17 is ID
y = train.iloc[:, 11]

X_test = test.iloc[:, [3, 4, 5, 8, 9, 10, 11, 12, 13, 14, 15]]

X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.85, test_size=0.15)

rf_model = RandomForestRegressor(n_estimators=100)
rf_model.fit(X_train, y_train)

y_pred_validation = rf_model.predict(X_validation)
# yhat = rf_model.predict(X_test)

# plotting feature importances
'''
var_label, var_importance = X_train.columns, rf_model.feature_importances_

plt.figure(figsize=(30,15))
plt.barh(var_label, var_importance)
plt.tick_params(labelsize=25)
'''


def score_fun(y_pred, y_true):
    return np.sqrt(np.mean((np.log(y_pred + 1) - np.log(y_true + 1))**2))


score_validation = score_fun(y_pred_validation, y_validation)
print(score_validation)         # 0.03916

# df_grouped = raggruppa(test, yhat)
# crea_file_submission(df_grouped, 6)
